-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 08, 2023 at 03:50 AM
-- Server version: 8.0.30
-- PHP Version: 8.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `test_ptski`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_reset_tokens_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2023_12_07_073844_create_barangs_table', 1),
(6, '2023_12_07_074239_create_supliers_table', 1),
(7, '2023_12_07_074517_create_hpembelians_table', 1),
(8, '2023_12_07_074750_create_dpembelians_table', 1),
(9, '2023_12_07_080508_create_stocks_table', 1),
(10, '2023_12_07_080859_create_hutangs_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_reset_tokens`
--

CREATE TABLE `password_reset_tokens` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_barang`
--

CREATE TABLE `tbl_barang` (
  `id` bigint UNSIGNED NOT NULL,
  `kodebrg` char(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `namabrg` char(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `satuan` char(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hargabeli` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_barang`
--

INSERT INTO `tbl_barang` (`id`, `kodebrg`, `namabrg`, `satuan`, `hargabeli`, `created_at`, `updated_at`) VALUES
(1, 'B202312001', 'Monitor', 'pcs', 2000000, NULL, NULL),
(2, 'B202312002', 'Speaker', 'pcs', 500000, NULL, NULL),
(3, 'B202312003', 'Keyboard', 'pcs', 125000, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_dbeli`
--

CREATE TABLE `tbl_dbeli` (
  `id` bigint UNSIGNED NOT NULL,
  `notransaksi` char(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kodebrg` char(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hargabeli` int NOT NULL,
  `qty` int NOT NULL,
  `diskon` int NOT NULL,
  `diskonrp` int NOT NULL,
  `totalrp` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_dbeli`
--

INSERT INTO `tbl_dbeli` (`id`, `notransaksi`, `kodebrg`, `hargabeli`, `qty`, `diskon`, `diskonrp`, `totalrp`, `created_at`, `updated_at`) VALUES
(1, 'T202312001', 'B202312001', 1899000, 1, 0, 0, 1899000, NULL, NULL),
(2, 'T202312001', 'B202312002', 479000, 1, 0, 0, 479000, NULL, NULL),
(3, 'T202312002', 'B202312002', 479000, 3, 0, 0, 1437000, NULL, NULL),
(4, 'T202312003', 'B202312001', 1899000, 10, 15, 2848500, 16141500, NULL, NULL),
(5, 'T202312003', 'B202312003', 100000, 10, 5, 50000, 950000, NULL, NULL),
(6, 'T202312004', 'B202312001', 1700000, 1, 0, 0, 1700000, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_hbeli`
--

CREATE TABLE `tbl_hbeli` (
  `id` bigint UNSIGNED NOT NULL,
  `notransaksi` char(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kodespl` char(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tglbeli` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_hbeli`
--

INSERT INTO `tbl_hbeli` (`id`, `notransaksi`, `kodespl`, `tglbeli`, `created_at`, `updated_at`) VALUES
(1, 'T202312001', 'SUP001', '2023-12-08 00:00:00', NULL, NULL),
(2, 'T202312002', 'SUP001', '2023-12-08 00:00:00', NULL, NULL),
(3, 'T202312003', 'SUP001', '2023-12-08 00:00:00', NULL, NULL),
(4, 'T202312004', 'SUP002', '2023-12-08 00:00:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_hutang`
--

CREATE TABLE `tbl_hutang` (
  `id` bigint UNSIGNED NOT NULL,
  `notransaksi` char(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kodespl` char(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tglbeli` datetime NOT NULL,
  `totalhutang` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_hutang`
--

INSERT INTO `tbl_hutang` (`id`, `notransaksi`, `kodespl`, `tglbeli`, `totalhutang`, `created_at`, `updated_at`) VALUES
(1, 'T202312001', 'SUP001', '2023-12-08 00:00:00', 2378000, NULL, NULL),
(2, 'T202312002', 'SUP001', '2023-12-08 00:00:00', 1437000, NULL, NULL),
(3, 'T202312003', 'SUP001', '2023-12-08 00:00:00', 17091500, NULL, NULL),
(4, 'T202312004', 'SUP002', '2023-12-08 00:00:00', 1700000, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_stock`
--

CREATE TABLE `tbl_stock` (
  `kodebrg` char(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `qtybeli` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_stock`
--

INSERT INTO `tbl_stock` (`kodebrg`, `qtybeli`, `created_at`, `updated_at`) VALUES
('B202312001', 12, NULL, NULL),
('B202312002', 4, NULL, NULL),
('B202312003', 10, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_suplier`
--

CREATE TABLE `tbl_suplier` (
  `id` bigint UNSIGNED NOT NULL,
  `kodespl` char(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `namaspl` char(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_suplier`
--

INSERT INTO `tbl_suplier` (`id`, `kodespl`, `namaspl`, `created_at`, `updated_at`) VALUES
(1, 'SUP001', 'Excel Store', NULL, NULL),
(2, 'SUP002', 'Via Store', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_reset_tokens`
--
ALTER TABLE `password_reset_tokens`
  ADD PRIMARY KEY (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `tbl_barang`
--
ALTER TABLE `tbl_barang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_dbeli`
--
ALTER TABLE `tbl_dbeli`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_hbeli`
--
ALTER TABLE `tbl_hbeli`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_hutang`
--
ALTER TABLE `tbl_hutang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_stock`
--
ALTER TABLE `tbl_stock`
  ADD UNIQUE KEY `tbl_stock_kodebrg_unique` (`kodebrg`);

--
-- Indexes for table `tbl_suplier`
--
ALTER TABLE `tbl_suplier`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_barang`
--
ALTER TABLE `tbl_barang`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_dbeli`
--
ALTER TABLE `tbl_dbeli`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_hbeli`
--
ALTER TABLE `tbl_hbeli`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_hutang`
--
ALTER TABLE `tbl_hutang`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_suplier`
--
ALTER TABLE `tbl_suplier`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
