<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('login');
});

Route::get('dashboard', function () {
    return view('dashboard');
})->name('dashboard');

Route::get('barang', function () {
    return view('barang.index');
})->name('barang');
Route::get('barang/{id}', function () {
    return view('barang.edit');
})->name('edit-barang');
Route::get('daftar-barang', function () {
    return view('barang.list');
})->name('daftar-barang');

Route::get('suplier', function () {
    return view('suplier.index');
})->name('suplier');
Route::get('suplier/{id}', function () {
    return view('suplier.edit');
})->name('edit-suplier');
Route::get('daftar-suplier', function () {
    return view('suplier.list');
})->name('daftar-suplier');

Route::get('pembelian', function () {
    return view('pembelian.index');
})->name('pembelian');
Route::get('pembelian/{id}', function () {
    return view('pembelian.edit');
})->name('edit-pembelian');
Route::get('daftar-pembelian', function () {
    return view('pembelian.list');
})->name('daftar-pembelian');
