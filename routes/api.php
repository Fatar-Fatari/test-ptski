<?php

use App\Http\Controllers\BarangController;
use App\Http\Controllers\PembelianController;
use App\Http\Controllers\SuplierController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('barang', [BarangController::class, 'index']);
Route::post('barang', [BarangController::class, 'create']);
Route::get('barang/{id}', [BarangController::class, 'show']);
Route::put('barang/{id}', [BarangController::class, 'update']);
Route::delete('barang/{id}', [BarangController::class, 'delete']);

Route::get('suplier', [SuplierController::class, 'index']);
Route::post('suplier', [SuplierController::class, 'create']);
Route::get('suplier/{id}', [SuplierController::class, 'show']);
Route::put('suplier/{id}', [SuplierController::class, 'update']);
Route::delete('suplier/{id}', [SuplierController::class, 'delete']);

Route::get('pembelian', [PembelianController::class, 'index']);
Route::post('pembelian', [PembelianController::class, 'create']);
Route::get('pembelian/{id}', [PembelianController::class, 'show']);
Route::put('pembelian/{id}', [PembelianController::class, 'update']);
Route::delete('pembelian/{id}', [PembelianController::class, 'delete']);
