<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $barang = [
            [
                'kodebrg' => 'A1',
                'namabrg' => 'Monitor LG',
                'satuan' => 'pcs',
                'hargabeli' => '1500000',
            ],
            [
                'kodebrg' => 'A2',
                'namabrg' => 'Speaker RGB',
                'satuan' => 'pcs',
                'hargabeli' => '50000',
            ],
            [
                'kodebrg' => 'A3',
                'namabrg' => 'Deskmate',
                'satuan' => 'pcs',
                'hargabeli' => '62000',
            ],
            [
                'kodebrg' => 'B2',
                'namabrg' => 'Combo Mouse + Keyboard',
                'satuan' => 'pcs',
                'hargabeli' => '239000',
            ],
        ];
        foreach ($barang as $item) {
            DB::table('tbl_barang')->insert($item);
        }

        $suplier = [
            [
                'kodespl' => 'SUP001',
                'namaspl' => 'John Doe',
            ],
            [
                'kodespl' => 'SUP002',
                'namaspl' => 'David Blake',
            ],
        ];
        foreach ($suplier as $item) {
            DB::table('tbl_suplier')->insert($item);
        }
    }
}
