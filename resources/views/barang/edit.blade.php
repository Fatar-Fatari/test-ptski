@extends('app')

@section('content')
    <div class="card shadow mb-4">
        <div class="card-header">
            <a href="{{ route('daftar-barang') }}">Kembali ke daftar barang</a>
        </div>
        <div class="card-body">
            <div class="card-title font-weight-bold mb-4">Edit data barang</div>

            <div class="card border-left-danger mb-4 d-none" id="error-card">
                <div class="card-body">
                    <div class="card-title font-weight-bold text-danger"></div>
                    <ul class="text-danger"></ul>
                </div>
            </div>
            <div class="alert alert-success alert-dismissible fade show d-none" role="alert" id="success-alert">
                <strong></strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form id="form-submit">
                @csrf
                <input type="hidden" name="kodebrg">
                <div class="row">
                    <div class="col">
                        <div class="mb-3">
                            <label for="namabrg">Nama Barang</label>
                            <input type="text" class="form-control" name="namabrg" id="namabrg">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="mb-3">
                            <label for="satuan">Satuan</label>
                            <input type="text" class="form-control" name="satuan" id="satuan">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="mb-3">
                            <label for="hargabeli">Harga Beli</label>
                            <input type="number" class="form-control" name="hargabeli" id="hargabeli" min="0">
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary w-100 mt-4">Submit</button>
            </form>
        </div>
    </div>
@endsection

@push('script')
    <script>
        const pathArray = window.location.pathname.split("/")
        const id = pathArray[2]

        $.ajax({
            type: 'get',
            url: `{{ url('api/barang') }}/${id}`,
            success: function(response) {
                $('#form-submit').find('input[name="kodebrg"]').val(response.data.kodebrg)
                $('#form-submit').find('input[name="namabrg"]').val(response.data.namabrg)
                $('#form-submit').find('input[name="satuan"]').val(response.data.satuan)
                $('#form-submit').find('input[name="hargabeli"]').val(response.data.hargabeli)
            },
        });

        $(document).ready(function() {
            $('#success-alert').hide().removeClass('d-none')
        })

        $(document).on('submit', '#form-submit', function(e) {
            e.preventDefault()
            let form = $('#form-submit')
            let formData = form.serialize()
            let btnSubmit = form.find('button[type="submit"]')
            let errorCard = $('#error-card')
            let successAlert = $('#success-alert')

            $.ajax({
                type: 'put',
                url: `{{ url('api/barang') }}/${id}`,
                data: formData,
                beforeSend: function() {
                    btnSubmit.addClass('disabled')
                },
                success: function(response) {
                    console.log(response)
                    errorCard.removeClass('d-block').addClass('d-none')
                    successAlert.find('strong').html(response.message)
                    successAlert.fadeTo(3000, 500).slideUp(500, function() {
                        successAlert.slideUp(500);
                    });
                },
                error: function(xhr, status, error) {
                    errorResp = xhr.responseJSON

                    errorCard.removeClass('d-none').addClass('d-block')
                    errorCard.find('.card-title').html(errorResp.message)
                    errorCard.find('ul').empty()
                    $.each(errorResp.data, function(key, value) {
                        errorCard.find('ul').append(`<li>${value}</li>`)
                    })
                },
                complete: function() {
                    btnSubmit.removeClass('disabled')
                }
            });
        })
    </script>
@endpush
