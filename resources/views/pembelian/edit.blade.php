@extends('app')

@section('content')
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Detail Pembelian</h6>
        </div>
        <div class="card-body">
            <div class="alert alert-success alert-dismissible fade show d-none" role="alert" id="success-alert">
                <strong>Message</strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Nama Barang</th>
                            <th>Harga Beli</th>
                            <th>Qty</th>
                            <th>Diskon (%)</th>
                            <th>Diskon (Rp)</th>
                            <th>Total (Rp)</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script>
        const pathArray = window.location.pathname.split("/")
        const id = pathArray[2]

        $(document).ready(function() {
            $('#success-alert').hide().removeClass('d-none')

            let datatable = $('#dataTable').DataTable({
                ajax: `{{ url('api/pembelian') }}/${id}`,
                columns: [{
                        data: 'namabrg'
                    },
                    {
                        data: 'hargabeli'
                    },
                    {
                        data: 'qty'
                    },
                    {
                        data: 'diskon'
                    },
                    {
                        data: 'diskonrp'
                    },
                    {
                        data: 'totalrp'
                    },
                ]
            });
        });
    </script>
@endpush
