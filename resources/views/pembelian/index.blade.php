@extends('app')

@section('content')
    <div class="card shadow mb-4">
        <div class="card-body">
            <div class="card-title font-weight-bold mb-4">Tambah data pembelian</div>

            <div class="card border-left-danger mb-4 d-none" id="error-card">
                <div class="card-body">
                    <div class="card-title font-weight-bold text-danger"></div>
                    <ul class="text-danger"></ul>
                </div>
            </div>

            <form id="form-submit">
                @csrf
                <div class="row">
                    <div class="col">
                        <div class="mb-3">
                            <label for="kodebrg">Barang</label>
                            <input type="hidden" name="namabrg">
                            <select class="form-control" name="kodebrg" id="kodebrg">
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="mb-3">
                            <label for="hargabeli">Harga Beli</label>
                            <input type="number" class="form-control" name="hargabeli" id="hargabeli">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="mb-3">
                            <label for="qty">Qty</label>
                            <input type="number" class="form-control" name="qty" id="qty" min="0">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="mb-3">
                            <label for="diskon">Diskon (%)</label>
                            <input type="number" class="form-control" name="diskon" id="diskon" min="0"
                                max="100" value="0">
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary w-100 mt-4">Tambahkan</button>
            </form>
        </div>
    </div>

    <div class="card shadow mb-4">
        <div class="card-body">
            <div class="mb-3">
                <label for="kodespl">Suplier</label>
                <select class="form-control" name="kodespl" id="kodespl">
                </select>
            </div>
            <table class="table table-bordered" id="dataTable">
                <thead>
                    <th></th>
                    <th>Nama Barang</th>
                    <th>Harga Beli</th>
                    <th>Qty</th>
                    <th>Diskon (%)</th>
                    <th>Diskon (Rp)</th>
                    <th>Total (Rp)</th>
                </thead>
                <tbody></tbody>
            </table>
            <p class="text-right font-weight-bold" id="grandtotal">Grand Total : </p>
            <button class="btn btn-primary btn-save">Save</button>
            <div class="alert alert-success alert-dismissible fade show mt-3 d-none" role="alert" id="success-alert">
                <strong>Message</strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script>
        const cart = []
        let grandTotal = 0;

        function saveCart(item) {
            cart.push(item);
        }

        $(document).ready(function() {
            $('#success-alert').hide().removeClass('d-none')

            const datatable = $('#dataTable').DataTable({
                filtering: false,
                paging: false,
                searching: false,
                info: false,
                data: cart,
                columns: [{
                        render: function(data, type, row, meta) {
                            return `<button class="btn btn-sm btn-dark btn-hapus" data-kodebrg="${row.kodebrg}">Hapus</button>`
                        }
                    },
                    {
                        data: 'namabrg'
                    },
                    {
                        data: 'hargabeli'
                    },
                    {
                        data: 'qty'
                    },
                    {
                        data: 'diskon'
                    },
                    {
                        data: 'diskonrp'
                    },
                    {
                        data: 'totalrp'
                    }
                ]
            });

            function calculateGrandTotal() {
                let total = 0;
                for (const item of cart) {
                    total += item.totalrp;
                }
                grandTotal = total

                $('#grandtotal').empty().html('Grand total : ' + grandTotal)
                if (grandTotal == 0) {
                    $('.btn-save').addClass('disabled')
                } else {
                    $('.btn-save').removeClass('disabled')
                }

                datatable.clear().rows.add(cart).draw();
            }

            calculateGrandTotal()

            $.ajax({
                type: 'get',
                url: `{{ url('api/barang') }}`,
                success: function(response) {
                    $.each(response.data, function(key, item) {
                        $('#form-submit').find('select[name="kodebrg"]')
                            .append(
                                `<option value="${item.kodebrg}" data-namabrg="${item.namabrg}">${item.namabrg}</option>`
                            )
                    })
                    let namabrg = $('#kodebrg :selected').data('namabrg')
                    $('#form-submit').find('input[name="namabrg"]').val(namabrg)
                },
            });
            $.ajax({
                type: 'get',
                url: `{{ url('api/suplier') }}`,
                success: function(response) {
                    $.each(response.data, function(key, item) {
                        $('select[name="kodespl"]')
                            .append(
                                `<option value="${item.kodespl}">${item.namaspl}</option>`
                            )
                    })
                },
            });

            $(document).on('change', '#kodebrg', function(e) {
                let namabrg = $('#kodebrg :selected').data('namabrg')
                $('#form-submit').find('input[name="namabrg"]').val(namabrg)
            })

            $(document).on('submit', '#form-submit', function(e) {
                e.preventDefault()
                let form = $('#form-submit')
                let kodebrg = form.find('select[name="kodebrg"]').val()
                let namabrg = form.find('input[name="namabrg"]').val()
                let hargabeli = form.find('input[name="hargabeli"]').val()
                let qty = form.find('input[name="qty"]').val()
                let diskon = form.find('input[name="diskon"]').val()
                let diskonrp = (hargabeli * qty) * (diskon / 100)
                let totalrp = (hargabeli * qty) - diskonrp

                let item = {
                    kodebrg: kodebrg,
                    namabrg: namabrg,
                    hargabeli: hargabeli,
                    qty: qty,
                    diskon: diskon,
                    diskonrp: diskonrp,
                    totalrp: totalrp,
                }

                console.log(cart);
                saveCart(item)
                calculateGrandTotal()
            })

            $(document).on('click', '.btn-hapus', function(e) {
                let kodebrg = $(this).data('kodebrg');

                const indexToRemove = cart.findIndex(item => item.kodebrg === kodebrg);

                if (confirm('Hapus item dari daftar')) {
                    if (indexToRemove !== -1) {
                        cart.splice(indexToRemove, 1);
                        calculateGrandTotal()
                    }
                }
            })

            $(document).on('click', '.btn-save', function(e) {
                let successAlert = $('#success-alert')
                let kodespl = $('#kodespl').val()

                $.ajax({
                    type: 'POST',
                    url: "{{ url('api/pembelian') }}",
                    data: {
                        kodespl: kodespl,
                        grandtotal: grandTotal,
                        items: cart
                    },
                    beforeSend: function() {
                        $('.btn-save').addClass('disabled')
                    },
                    success: function(response) {
                        console.log(response)
                        // errorCard.removeClass('d-block').addClass('d-none')
                        successAlert.find('strong').html(response.message)
                        successAlert.fadeTo(3000, 500).slideUp(500, function() {
                            successAlert.slideUp(500);
                        });
                        while (cart.length) {
                            cart.pop();
                        }
                        calculateGrandTotal()
                        // form.trigger('reset')
                    },
                    error: function(xhr, status, error) {
                        errorResp = xhr.responseJSON
                        alert(error);
                    },
                    complete: function() {
                        $('.btn-save').removeClass('disabled')
                    }
                });
            })
        })
    </script>
@endpush
