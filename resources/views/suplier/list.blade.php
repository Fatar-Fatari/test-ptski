@extends('app')

@section('content')
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Daftar Suplier</h6>
        </div>
        <div class="card-body">
            <div class="alert alert-success alert-dismissible fade show d-none" role="alert" id="success-alert">
                <strong>Message</strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Kode Suplier</th>
                            <th>Nama Suplier</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script>
        $(document).ready(function() {
            $('#success-alert').hide().removeClass('d-none')

            let datatable = $('#dataTable').DataTable({
                ajax: "{{ url('api/suplier') }}",
                columns: [{
                        data: 'kodespl'
                    },
                    {
                        data: 'namaspl'
                    },
                    {
                        render: function(data, type, row, meta) {
                            return `<a href="{{ url('suplier') }}/${row.id}" class="btn btn-sm btn-light mr-1">Edit</a>
                                    <button class="btn btn-sm btn-dark btn-hapus" data-id="${row.id}">Hapus</button>`
                        }
                    }
                ]
            });

            $(document).on('click', '.btn-hapus', function() {
                let id = $(this).data('id')
                if (confirm('Hapus data ini ?')) {
                    $.ajax({
                        url: `{{ url('api/suplier') }}/${id}`,
                        type: 'delete',
                        success: function(response) {
                            $('#success-alert').find('strong').html(response.message)
                            $('#success-alert').fadeTo(3000, 500).slideUp(500, function() {
                                $('#success-alert').slideUp(500);
                            });
                            datatable.ajax.reload()
                        }
                    });
                }
            })
        });
    </script>
@endpush
