@extends('app')

@section('content')
    <div class="card shadow mb-4">
        <div class="card-header">
            <a href="{{ route('daftar-suplier') }}">Kembali ke daftar suplier</a>
        </div>
        <div class="card-body">
            <div class="card-title font-weight-bold mb-4">Edit data suplier</div>

            <div class="card border-left-danger mb-4 d-none" id="error-card">
                <div class="card-body">
                    <div class="card-title font-weight-bold text-danger"></div>
                    <ul class="text-danger"></ul>
                </div>
            </div>
            <div class="alert alert-success alert-dismissible fade show d-none" role="alert" id="success-alert">
                <strong></strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form id="form-submit">
                @csrf
                <input type="hidden" name="kodespl">
                <div class="row">
                    <div class="col">
                        <div class="mb-3">
                            <label for="namaspl">Nama Suplier</label>
                            <input type="text" class="form-control" name="namaspl" id="namaspl">
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary w-100 mt-4">Submit</button>
            </form>
        </div>
    </div>
@endsection

@push('script')
    <script>
        const pathArray = window.location.pathname.split("/")
        const id = pathArray[2]

        $.ajax({
            type: 'get',
            url: `{{ url('api/suplier') }}/${id}`,
            success: function(response) {
                $('#form-submit').find('input[name="kodespl"]').val(response.data.kodespl)
                $('#form-submit').find('input[name="namaspl"]').val(response.data.namaspl)
            },
        });

        $(document).ready(function() {
            $('#success-alert').hide().removeClass('d-none')
        })

        $(document).on('submit', '#form-submit', function(e) {
            e.preventDefault()
            let form = $('#form-submit')
            let formData = form.serialize()
            let btnSubmit = form.find('button[type="submit"]')
            let errorCard = $('#error-card')
            let successAlert = $('#success-alert')

            $.ajax({
                type: 'put',
                url: `{{ url('api/suplier') }}/${id}`,
                data: formData,
                beforeSend: function() {
                    btnSubmit.addClass('disabled')
                },
                success: function(response) {
                    console.log(response)
                    errorCard.removeClass('d-block').addClass('d-none')
                    successAlert.find('strong').html(response.message)
                    successAlert.fadeTo(3000, 500).slideUp(500, function() {
                        successAlert.slideUp(500);
                    });
                },
                error: function(xhr, status, error) {
                    errorResp = xhr.responseJSON

                    errorCard.removeClass('d-none').addClass('d-block')
                    errorCard.find('.card-title').html(errorResp.message)
                    errorCard.find('ul').empty()
                    $.each(errorResp.data, function(key, value) {
                        errorCard.find('ul').append(`<li>${value}</li>`)
                    })
                },
                complete: function() {
                    btnSubmit.removeClass('disabled')
                }
            });
        })
    </script>
@endpush
