@extends('app')

@section('page-heading', 'Dashboard')

@section('content')
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <div class="card-title font-weight-bold">
                Hai, Administrator
            </div>
            <p class="m-0">Selamat datang di dashboard.</p>
        </div>
    </div>
@endsection
