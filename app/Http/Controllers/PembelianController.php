<?php

namespace App\Http\Controllers;

use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class PembelianController extends Controller
{
    public function index()
    {
        $pembelian = DB::table('tbl_hbeli')
            ->leftJoin('tbl_suplier', 'tbl_hbeli.kodespl', '=', 'tbl_suplier.kodespl')
            ->select('tbl_hbeli.*', 'tbl_suplier.namaspl')
            ->orderBy('tbl_hbeli.notransaksi')->get();
        return response()->json([
            'data' => $pembelian
        ]);
    }


    public function create(Request $request)
    {
        $latest_data = DB::table('tbl_hbeli')->orderBy('notransaksi', 'desc')->first();
        if (!$latest_data) {
            $notransaksi = 'T' . date('Y') . date('m') . '001';
        } else {
            $latest_kode = substr($latest_data->notransaksi, -3);
            $next_kode = str_pad($latest_kode + 1, 3, '0', STR_PAD_LEFT);
            $notransaksi = 'T' . date('Y') . date('m') . $next_kode;
        }

        $kodespl = $request->kodespl;
        $items = $request->items;

        try {
            DB::table('tbl_hbeli')->insert([
                'notransaksi' => $notransaksi,
                'kodespl' => $kodespl,
                'tglbeli' => date('Y-m-d H:i:s')
            ]);

            foreach ($items as $item) {
                $diskonrp = ($item['hargabeli'] * $item['qty']) * ($item['diskon'] / 100);
                $totalrp = ($item['hargabeli'] * $item['qty']) - $diskonrp;
                DB::table('tbl_dbeli')->insert([
                    'notransaksi' => $notransaksi,
                    'kodebrg' => $item['kodebrg'],
                    'hargabeli' => $item['hargabeli'],
                    'qty' => $item['qty'],
                    'diskon' => $item['diskon'],
                    'diskonrp' => $diskonrp,
                    'totalrp' => $totalrp,
                ]);

                $qtybeli = $item['qty'];
                DB::table('tbl_stock')->upsert(
                    ['kodebrg' => $item['kodebrg'], 'qtybeli' => $qtybeli,],
                    'kodebrg',
                    ['qtybeli' => DB::raw("tbl_stock.qtybeli + $qtybeli")]
                );
            }

            DB::table('tbl_hutang')->insert([
                'notransaksi' => $notransaksi,
                'kodespl' => $kodespl,
                'tglbeli' => date('Y-m-d H:i:s'),
                'totalhutang' => $request->grandtotal
            ]);

            return response()->json([
                'success'   => true,
                'message'   => 'Data saved successfully',
                'data'      => ''
            ]);
        } catch (\Throwable $th) {
            throw new HttpResponseException(response()->json([
                'success'   => false,
                'message'   => 'Insert errors',
                'data'      => ''
            ], 500));
        }
    }


    public function show($notransaksi)
    {
        $pembelian = DB::table('tbl_dbeli')
            ->leftJoin('tbl_barang', 'tbl_dbeli.kodebrg', '=', 'tbl_barang.kodebrg')
            ->select('tbl_dbeli.*', 'tbl_barang.namabrg')
            ->where('notransaksi', $notransaksi)
            ->orderBy('tbl_dbeli.notransaksi')
            ->get();
        return response()->json([
            'success'   => true,
            'message'   => 'Show data',
            'data'      => $pembelian
        ]);
    }


    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'namabrg' => 'required',
            'satuan' => 'required',
            'hargabeli' => 'required',
        ]);

        if ($validator->fails()) {
            throw new HttpResponseException(response()->json([
                'success'   => false,
                'message'   => 'Validation errors',
                'data'      => $validator->errors()
            ], 500));
        }

        $data = $validator->validated();

        DB::table('tbl_hbeli')->where('id', $id)->update($data);
        return response()->json([
            'success'   => true,
            'message'   => 'Data created successfully',
            'data'      => $data
        ]);
    }


    public function delete($id)
    {
        DB::table('tbl_hbeli')->where('id', $id)->delete();
        return response()->json([
            'success'   => true,
            'message'   => 'Data deleted successfully',
            'data'      => ''
        ]);
    }
}
