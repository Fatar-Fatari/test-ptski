<?php

namespace App\Http\Controllers;

use App\Models\Barang;
use App\Models\Stock;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class BarangController extends Controller
{
    public function index()
    {
        $barang = DB::table('tbl_barang')
            ->leftJoin('tbl_stock', 'tbl_barang.kodebrg', '=', 'tbl_stock.kodebrg')
            ->select('tbl_barang.*', 'tbl_stock.qtybeli')
            ->orderBy('tbl_barang.kodebrg')->get();
        return response()->json([
            'data' => $barang
        ]);
    }


    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'namabrg' => 'required',
            'satuan' => 'required',
            'hargabeli' => 'required',
        ]);

        if ($validator->fails()) {
            throw new HttpResponseException(response()->json([
                'success'   => false,
                'message'   => 'Validation errors',
                'data'      => $validator->errors()
            ], 500));
        }

        $data = $validator->validated();

        $latest_data = DB::table('tbl_barang')->orderBy('kodebrg', 'desc')->first();
        if (!$latest_data) {
            $data['kodebrg'] = 'B' . date('Y') . date('m') . '001';
        } else {
            $latest_kode = substr($latest_data->kodebrg, -3);
            $next_kode = str_pad($latest_kode + 1, 3, '0', STR_PAD_LEFT);
            $data['kodebrg'] = 'B' . date('Y') . date('m') . $next_kode;
        }

        DB::table('tbl_barang')->insert($data);
        return response()->json([
            'success'   => true,
            'message'   => 'Data created successfully',
            'data'      => $data
        ]);
    }


    public function show($id)
    {
        $barang = DB::table('tbl_barang')->find($id);
        return response()->json([
            'success'   => true,
            'message'   => 'Show data',
            'data'      => $barang
        ]);
    }


    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'namabrg' => 'required',
            'satuan' => 'required',
            'hargabeli' => 'required',
        ]);

        if ($validator->fails()) {
            throw new HttpResponseException(response()->json([
                'success'   => false,
                'message'   => 'Validation errors',
                'data'      => $validator->errors()
            ], 500));
        }

        $data = $validator->validated();

        DB::table('tbl_barang')->where('id', $id)->update($data);
        return response()->json([
            'success'   => true,
            'message'   => 'Data created successfully',
            'data'      => $data
        ]);
    }


    public function delete($id)
    {
        DB::table('tbl_barang')->where('id', $id)->delete();
        return response()->json([
            'success'   => true,
            'message'   => 'Data deleted successfully',
            'data'      => ''
        ]);
    }
}
