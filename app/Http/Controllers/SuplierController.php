<?php

namespace App\Http\Controllers;

use App\Models\Suplier;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class SuplierController extends Controller
{
    public function index()
    {
        $suplier = DB::table('tbl_suplier')->orderBy('kodespl')->get();
        return response()->json([
            'data' => $suplier
        ]);
    }


    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'namaspl' => 'required',
        ]);

        if ($validator->fails()) {
            throw new HttpResponseException(response()->json([
                'success'   => false,
                'message'   => 'Validation errors',
                'data'      => $validator->errors()
            ], 500));
        }

        $data = $validator->validated();

        $latest_data = DB::table('tbl_suplier')->orderBy('kodespl', 'desc')->first();
        if (!$latest_data) {
            $data['kodespl'] = 'SUP' . '001';
        } else {
            $latest_id = substr($latest_data->id, -3);
            $next_id = str_pad($latest_id + 1, 3, '0', STR_PAD_LEFT);
            $data['kodespl'] = 'SUP' . $next_id;
        }

        DB::table('tbl_suplier')->insert($data);
        return response()->json([
            'success'   => true,
            'message'   => 'Data created successfully',
            'data'      => $data
        ]);
    }


    public function show($id)
    {
        $suplier = DB::table('tbl_suplier')->find($id);
        return response()->json([
            'success'   => true,
            'message'   => 'Show data',
            'data'      => $suplier
        ]);
    }


    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'namaspl' => 'required',
        ]);

        if ($validator->fails()) {
            throw new HttpResponseException(response()->json([
                'success'   => false,
                'message'   => 'Validation errors',
                'data'      => $validator->errors()
            ], 500));
        }

        $data = $validator->validated();

        DB::table('tbl_suplier')->where('id', $id)->update($data);
        return response()->json([
            'success'   => true,
            'message'   => 'Data created successfully',
            'data'      => $data
        ]);
    }


    public function delete($id)
    {
        DB::table('tbl_suplier')->where('id', $id)->delete();
        return response()->json([
            'success'   => true,
            'message'   => 'Data deleted successfully',
            'data'      => ''
        ]);
    }
}
